interface SignInStrings {
  [key: string]: {
    fill: string
    email: string
    placeholderEmail: string
    pass: string
    placeholderPass: string
    forgotPassword: string
    signUp: string
    signIn: string
    invalidField: string
    badgeClient: string
    badgeAdmin: string,
    targetText: string
  }
}

const strings: SignInStrings = {
  'PT': {
    fill: 'Para acessar, faça seu login',
    email: 'E-mail*',
    placeholderEmail: 'Digite seu e-mail',
    pass: 'Senha*',
    placeholderPass: 'Digite sua senha',
    forgotPassword: 'Esqueci minha senha',
    signUp: 'Cadastrar-se',
    signIn: 'Entrar',
    invalidField: 'Campo Inválido',
    badgeClient: 'Cliente',
    badgeAdmin: 'Administrador',
    targetText: 'Selecione qual visão você deseja acessar'
  },
  'EN': {
    fill: 'To access, please login',
    email: 'E-mail*',
    placeholderEmail: 'Type your e-mail',
    pass: 'Password*',
    placeholderPass: 'Type your password',
    forgotPassword: 'Forgot my password',
    signUp: 'Sign up',
    signIn: 'Sign In',
    invalidField: 'Invalid Field',
    badgeClient: 'Client',
    badgeAdmin: 'Administrator',
    targetText: 'Select which view you want to access'
  },
  'ES': {
    fill: 'Para acceder, inicie sesión',
    email: 'Correo electrónico*',
    placeholderEmail: 'Ingresa tu correo electrónico',
    pass: 'Contraseña*',
    placeholderPass: 'Ingresa tu contraseña',
    forgotPassword: 'Olvide mi contraseña',
    signUp: 'Registrarse',
    signIn: 'Iniciar sesión',
    invalidField: 'Campo Inválido',
    badgeClient: 'Cliente',
    badgeAdmin: 'Administrador',
    targetText: 'Seleccione la vista a la que desea acceder'
  }
}

export default strings