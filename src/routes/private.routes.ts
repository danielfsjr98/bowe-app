import Calculator from '../views/Calculator';
import Home from '../views/Home';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/calculator',
    name: 'Calculator',
    component: Calculator,
  },
];

export default routes;
