import SignIn from '../views/SignIn';

const routes = [
  {
    path: '/login',
    name: 'SignIn',
    component: SignIn,
  },
];

export default routes;
