import {Switch, Route, Redirect} from 'react-router-dom';
import {useAuth} from '../contexts/AuthContext';
import {BaseLayout} from '../components/Layouts/BaseLayout';
import publicRoutes from './public.routes';
import privateRoutes from './private.routes';

const Routes = () => {
  const {signed} = useAuth();

  const routes = signed ? privateRoutes : publicRoutes;

  return (
    <BaseLayout>
      <Switch>
        {routes.map(route => (
          <Route
            key={route.name}
            path={route.path}
            component={route.component}
            exact
          />
        ))}
        <Redirect to={signed ? '/' : '/login'} />
      </Switch>
    </BaseLayout>
  );
};

export default Routes;
