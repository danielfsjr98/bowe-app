export interface AuthContextData {
  lang: string;
  setLang: React.Dispatch<React.SetStateAction<string>>;
  signed: boolean;
  signIn: (payload: SignInReq) => void;
}

export interface SignInReq {
  login: string;
  password: string;
}
