import React, {useEffect, useState} from 'react';
import * as I from './interfaces';
import {useHistory} from 'react-router';
import {BOWE_LOCAL_STORAGE_TOKEN} from '../../constants';

const AuthContext = React.createContext({} as I.AuthContextData);

// min * sec * ms
const EXP_TOKEN = 5 * 60 * 1000;

function validateToken() {
  const token = localStorage.getItem(BOWE_LOCAL_STORAGE_TOKEN);
  return !!token && parseInt(token) + EXP_TOKEN > new Date().valueOf();
}

export function AuthProvider({children}: {children: React.ReactNode}) {
  const history = useHistory();
  const [signed, setSigned] = useState(validateToken());
  const [lang, setLang] = useState('PT');

  useEffect(() => {
    if (!validateToken()) {
      localStorage.removeItem(BOWE_LOCAL_STORAGE_TOKEN);
    }
  }, []);

  async function signIn(payload: I.SignInReq) {
    try {
      localStorage.setItem(
        BOWE_LOCAL_STORAGE_TOKEN,
        new Date().valueOf().toString()
      );
      setSigned(true);
      history.push('/home');
    } catch (err) {}
  }

  return (
    <AuthContext.Provider
      value={{
        lang,
        setLang,
        signed,
        signIn,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export const useAuth = () => {
  return React.useContext(AuthContext);
};

export default AuthContext;
