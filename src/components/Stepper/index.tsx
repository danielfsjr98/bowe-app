import React from 'react';
import './styles.scss';
import * as I from './interfaces';
import {NavLink} from 'react-router-dom';

const Stepper: React.FC<I.StepperProps> = () => {
  return (
    <ul className='stepper nav nav-pills'>
      {/* Para step completo utilizar apenas a classe "complete" */}
      <NavLink
        className='stepper__item nav-item nav-link complete'
        exact
        to='/'
      >
        <div className='title mb-1'>Step 1</div>
        <div className='description'>Step description</div>
      </NavLink>

      <NavLink
        className='stepper__item nav-item nav-link complete'
        exact
        to='/'
      >
        <div className='title mb-1'>Step 2</div>
        <div className='description'>Step description</div>
      </NavLink>

      {/* Para step atual utilizar as classes "active" e "current" */}
      <NavLink
        className={`stepper__item nav-item nav-link`}
        exact
        activeClassName='active current'
        to='/'
      >
        <div className='title mb-1'>Simulação de desconto</div>
        <div className='description'>Veja sua simulação de desconto.</div>
      </NavLink>

      <NavLink
        className='stepper__item nav-item nav-link'
        exact
        activeClassName='active current'
        to='/calculator'
      >
        <div className='title mb-1'>Calculadora</div>
        <div className='description'>Calcule o desconto</div>
      </NavLink>

      <NavLink className='stepper__item nav-item nav-link' exact to='/'>
        <div className='title mb-1'>Step 5</div>
        <div className='description'>Step description</div>
      </NavLink>
    </ul>
  );
};

export default Stepper;
