import React from 'react';
import Logo from '../../../assets/images/logo-bowe.png';
import {ChangeLang} from '../../ChangeLang';
import './styles.scss';
import * as I from './interfaces';

export const SignInLayout: React.FC<I.SignInLayoutProps> = ({children}) => {
  return (
    <>
      <ChangeLang />
      <div className='d-flex col-6 w-100'>
        <div className='col-container'>
          <div className='content-form'>
            <img className='logo' src={Logo} alt='bowe' />
            {children}
          </div>
        </div>
        <div className='col-6 d-none d-lg-block'></div>
      </div>
    </>
  );
};
