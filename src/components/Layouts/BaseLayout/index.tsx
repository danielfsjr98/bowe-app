import React from 'react';
import './styles.scss';
import * as I from './interfaces';
import Header from '../../Header';
import {useAuth} from '../../../contexts/AuthContext';

export const BaseLayout: React.FC<I.BaseLayoutProps> = ({children}) => {
  const {signed} = useAuth();

  return (
    <div className={signed ? 'signed-container' : ''}>
      {signed && <Header />}

      {children}
    </div>
  );
};
