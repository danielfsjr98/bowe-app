export interface IconCardProps {
  text: string;
  icon: JSX.Element;
}