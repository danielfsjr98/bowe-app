import React from 'react';
import './styles.scss';
import * as I from './interfaces';

const IconCard: React.FC<I.IconCardProps> = ({icon, text}) => {
  return (
    <div className='d-flex flex-column fw-bold align-items-center bg-secondary px-5 py-3 rounded-4 w-100'>
      {icon}
      <h4 className='mt-2 mb-0 fw-bold text-center'>{text}</h4>
    </div>
  );
};

export default IconCard;
