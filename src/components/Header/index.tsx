import React from 'react';
import Stepper from '../Stepper';
import LogoBowe from '../../assets/images/logo-bowe.png';
import './styles.scss';

const Header: React.FC = () => {
  return (
    <div className='mb-4'>
      <header className='d-flex justify-content-between flex-column flex-md-row mb-3'>
        <h1 className='fw-bold'>A energia do futuro na palma de sua mão!</h1>
        <div>
          <img src={LogoBowe} alt='logo bowe' className='logo-container' />
        </div>
      </header>

      <Stepper />
    </div>
  );
};

export default Header;
