import React from 'react';
import {useAuth} from '../../contexts/AuthContext';
import './styles.scss';

export function ChangeLang() {
  const [showOptions, setShowOptions] = React.useState(false);
  const options = ['PT', 'EN', 'ES'];
  const {lang, setLang} = useAuth();

  return (
    <div className='block' onClick={() => setShowOptions(prev => !prev)}>
      {lang}
      {showOptions && (
        <div className='options'>
          {options
            .filter(el => el !== lang)
            .map(el => (
              <p key={el} onClick={() => setLang(el)}>
                {el}
              </p>
            ))}
        </div>
      )}
    </div>
  );
}
