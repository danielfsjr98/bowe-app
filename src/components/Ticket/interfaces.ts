export interface TicketProps {
  text: string;
  icon: JSX.Element;
}
