import React from 'react';
import './styles.scss';
import * as I from './interfaces';

const Ticket: React.FC<I.TicketProps> = ({text, icon}) => {
  return (
    <div className='container-ticket'>
      <div className='text-container'>
        <h4>{text}</h4>
      </div>
      <div className='icon-container'>
        <div className='circle-container'>{icon}</div>
      </div>
    </div>
  );
};

export default Ticket;
