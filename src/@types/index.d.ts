declare module '*.svg' {
  import React = require('react')
  const src: string
  export default src
}

declare module '*.png' {
  const content: string
  export default content
}

declare module "*.ttf" {
  const content: string;
  export default content;
}

declare module 'react-dropzone'