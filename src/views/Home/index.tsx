import React from 'react';
import BrazilMap from '../../assets/images/brazil_map.png';
import Ticket from '../../components/Ticket';
import {FaHandHoldingUsd, FaMobileAlt, FaPeopleCarry} from 'react-icons/fa';
import './styles.scss';

const Home: React.FC = () => {
  return (
    <div>
      <h3 className='text-secondary mb-4 text-center fw-bold'>
        Bluefit, você está a poucos passos de ter energia limpa e com desconto!
      </h3>

      <section>
        <h2 className='mb-2 text-center'>
          Veja sua simulação com 15% de desconto na tarifa:
        </h2>

        <div className='overflow-container'>
          <table>
            <thead>
              <tr>
                <th>CONCESSIONÁRIA</th>
                <th>UNIDADE</th>
                <th>NUMERO DA UNIDADE CONSUMIDORA</th>
                <th>REFERÊNCIA</th>
                <th>CUSTO ATUAL MENSAL</th>
                <th>ECONOMIA MENSAL</th>
                <th>ECONOMIA ANUAL</th>
                <th>Tarifa deReferencia com/sem Impostos</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>LIGHT</td>
                <td>FLAMENGO</td>
                <td>0410883023</td>
                <td>Junho/22</td>
                <td>R$ 15.734,27</td>
                <td>R$ 2.360,14</td>
                <td>R$ 28.321,68</td>
                <td>R$1,22161 / 0,8022</td>
              </tr>
              <tr>
                <td>LIGHT</td>
                <td>TIJUCA</td>
                <td>0412741294</td>
                <td>Junho/22</td>
                <td>R$12.949,02</td>
                <td>R$1.942,35</td>
                <td>R$23.308,23</td>
                <td>R$1,22161 / 0,8022</td>
              </tr>
              <tr>
                <td>LIGHT</td>
                <td>NILÓPOLIS</td>
                <td>0430130677</td>
                <td>Junho/22</td>
                <td>R$23.161,65</td>
                <td>R$3.474,24</td>
                <td>R$41.690,97</td>
                <td>R$1,22161 / 0,8022</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className='mt-1 fw-bold'>
          O desconto é calculado com base na tarifa de energia TE+TUSD.
          <br />
          A Bow-e nao oferece desconto na taxa de Iluminação publica, ao
          conferir o desconto certifique-se que não incluiu este custo.
          <br />
          Previsão de conexão maio/23.
        </div>
      </section>

      <section className='d-flex justify-content-between flex-column flex-md-row mt-5 overflow-container'>
        <div className='d-flex flex-column'>
          <div className='d-flex flex-column fw-bold'>
            <h4 className='mb-3'>Estamos expandindo para todo BRASIL!</h4>

            <h5 className='mb-2'>Veja onde já temos projetos em andamento:</h5>
          </div>
          <div className='d-flex justify-content-center'>
            <img src={BrazilMap} alt='Brazil map' className='img-container' />
          </div>
        </div>

        <div className='d-flex flex-column align-items-center'>
          <div className='mb-3'>
            <Ticket
              text='SEM INVESTIMENTO'
              icon={<FaHandHoldingUsd size={40} />}
            />
          </div>
          <div className='mb-3'>
            <Ticket
              text='100% DIGITAL E SEGURO'
              icon={<FaMobileAlt size={40} />}
            />
          </div>
          <div className='mb-3'>
            <Ticket text='SEM OBRAS' icon={<FaPeopleCarry size={40} />} />
          </div>
          <div className='text-center'>
            <h5>Data de Emissão: 28/07/2022</h5>
            <h5>Data de Validade: 28/08/2022</h5>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
