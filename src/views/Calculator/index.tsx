import React from 'react';
import {FaHome, FaStore, FaTractor} from 'react-icons/fa';
import IconCard from '../../components/IconCard';
import './styles.scss';

const Calculator: React.FC = () => {
  return (
    <div className='d-flex flex-column fw-bold align-items-center'>
      <h1 className='mb-3 text-center'>
        Calcule o quanto você vai economizar para realizar seus sonhos.
      </h1>
      <h2 className='mb-3'>QUER ENERGIA PARA:</h2>

      <div>
        <div className='d-flex flex-column gap-3 mb-4 flex-md-row w-100'>
          <IconCard text='Residência' icon={<FaHome size={50} />} />

          <IconCard text='Comércio' icon={<FaStore size={50} />} />

          <IconCard text='Zona Rural' icon={<FaTractor size={50} />} />
        </div>

        <div className='d-flex flex-column form-container align-items-center align-items-md-start'>
          <div className='d-flex align-items-center input-group mb-3 gap-3 w-75'>
            <label className='lead fw-bold text-center text-md-start'>
              Valor gasto em energia por mês:
            </label>
            <input
              type='text'
              className='form-control bg-info rounded-4 text-dark fw-bold border-0 shadow-none'
              placeholder='Informe o valor...'
              value=''
              readOnly
            />
          </div>

          <div className='d-flex align-items-center input-group mb-3 gap-3 w-50'>
            <label className='lead fw-bold text-center text-md-start'>
              Mês de referência:
            </label>
            <select
              className='form-select bg-info rounded-4 text-dark fw-bold border-0 shadow-none'
              placeholder='Selecione uma opção...'
              defaultValue='0'
            >
              <option value='0' disabled>Selecione uma opção</option>
              <option value='1'>One</option>
              <option value='2'>Two</option>
              <option value='3'>Three</option>
            </select>
          </div>

          <div className='d-flex align-items-center input-group mb-3 gap-3 w-25'>
            <label className='lead fw-bold text-center text-md-start'>
              Estado:
            </label>
            <select
              className='form-select bg-info rounded-4 text-dark fw-bold border-0 shadow-none'
              placeholder='Selecione uma opção...'
              defaultValue='0'
            >
              <option value='0' disabled>Selecione uma opção</option>
              <option value='1'>One</option>
              <option value='2'>Two</option>
              <option value='3'>Three</option>
            </select>
          </div>

          <div className='d-flex align-items-center input-group mb-3 gap-3 w-50'>
            <label className='lead fw-bold text-center text-md-start'>
              Concessionária:
            </label>
            <select
              className='form-select bg-info rounded-4 text-dark fw-bold border-0 shadow-none'
              placeholder='Selecione uma opção...'
              defaultValue='0'
            >
              <option value='0' disabled>Selecione uma opção</option>
              <option value='1'>One</option>
              <option value='2'>Two</option>
              <option value='3'>Three</option>
            </select>
          </div>

          <div className='d-flex align-items-center input-group mb-3 gap-3 w-100'>
            <label className='lead fw-bold text-center text-md-start'>
              Qual foi a média de energia nos últimos 3 meses:
            </label>
            <input
              type='text'
              className='form-control bg-info rounded-4 text-dark fw-bold border-0 shadow-none '
              placeholder='Informe o valor...'
              value=''
              readOnly
            />
          </div>

          <div className='d-flex align-items-center input-group mb-3 gap-3 w-50'>
            <label className='lead fw-bold text-center text-md-start'>
              Tipo de ligação:
            </label>
            <select
              className='form-select bg-info rounded-4 text-dark fw-bold border-0 shadow-none'
              placeholder='Selecione uma opção...'
              defaultValue='0'
            >
              <option value='0' disabled>Selecione uma opção</option>
              <option value='1'>One</option>
              <option value='2'>Two</option>
              <option value='3'>Three</option>
            </select>
          </div>
        </div>

        <div className='d-flex justify-content-center'>
          <div className='d-flex flex-column rounded-4 overflow-hidden mt-2 text-center'>
            <div className='bg-secondary px-5 py-2'>Desconto:</div>
            <div className='bg-info px-4 py-2 text-dark'>R$ 100, 00</div>
          </div>
        </div>
        <div className='d-flex justify-content-center'>
          <h4 className='mt-4 mb-3 bg-info text-center w-75 py-2 px-3 rounded-4 text-dark'>
            Faça upload de sua conta de luz
          </h4>
        </div>
      </div>
    </div>
  );
};

export default Calculator;
