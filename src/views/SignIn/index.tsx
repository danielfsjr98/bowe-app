import React, {useState} from 'react';
import strings from '../../@strings/SignIn';
import {Link} from 'react-router-dom';
import * as I from './interfaces';
import {useAuth} from '../../contexts/AuthContext';
import {isEmail} from '../../utils';
import {SignInLayout} from '../../components/Layouts/SignInLayout';
import {SignInReq} from '../../contexts/AuthContext/interfaces';

const SignIn = () => {
  const [form, setForm] = useState({
    login: '',
    password: '',
  } as SignInReq);
  const [error, setError] = useState({} as I.FormErrors);

  const {lang, signIn} = useAuth();

  const defaultError = strings[lang].invalidField;

  function handleInputChange({target}: {target: HTMLInputElement}) {
    if (target.value.trim() || target.value === '') {
      setForm((prev: SignInReq) => ({
        ...prev,
        [target.name]: target.value,
      }));
    }
  }

  function validForm(): boolean {
    let isValid = true;
    let localError = {} as I.FormErrors;

    if (!form.login || !isEmail(form.login.trim())) {
      localError = {
        ...localError,
        login: true,
      };
      isValid = false;
    }
    if (!form.password) {
      localError = {
        ...localError,
        password: true,
      };
      isValid = false;
    }

    setError(localError);
    return isValid;
  }

  function keyboardSubmit(e: React.KeyboardEvent<HTMLDivElement>) {
    if (e.key === 'Enter') {
      if (validForm()) signIn(form);
    }
  }

  function clickSubmit() {
    if (validForm()) signIn(form);
  }

  return (
    <SignInLayout>
      <h4 className='text-primary my-4'>{strings[lang].fill}</h4>

      <div
        className='w-100 d-flex flex-column align-items-center'
        onKeyPress={keyboardSubmit}
      >
        <div className='w-100 mb-2 mt-3 input-group d-flex flex-column'>
          <label htmlFor='login' className='form-label'>
            {strings[lang].email}
          </label>
          <input
            id='login'
            name={'login'}
            type={'text'}
            value={form.login}
            onChange={handleInputChange}
            placeholder={strings[lang].placeholderEmail}
            className='form-control text-dark shadow-none rounded-3 border-0 py-2 w-100'
          />
          {error.login && <div className='text-warning'>{defaultError}</div>}
        </div>
        <div className='w-100 my-3 input-group d-flex flex-column'>
          <label htmlFor='password' className='form-label'>
            {strings[lang].pass}
          </label>
          <input
            name={'password'}
            type={'password'}
            value={form.password}
            onChange={handleInputChange}
            placeholder={strings[lang].placeholderPass}
            className='form-control text-dark shadow-none rounded-3 border-0 py-2 w-100'
          />
          {error.password && <div className='text-warning'>{defaultError}</div>}
        </div>

        <Link to='/login'>
          <span>{strings[lang].forgotPassword}</span>
        </Link>

        <button
          onClick={clickSubmit}
          className='btn btn-secondary shadow-none rounded-3 py-2'
        >
          {strings[lang].signIn}
        </button>
      </div>

      <Link to='/login'>
        <span>{strings[lang].signUp}</span>
      </Link>
    </SignInLayout>
  );
};

export default SignIn;
